public class NumberPalindrome {
    public static boolean isPalindrome(int number) {
        int reverse = 0;
        int num = Math.abs(number);
        while(num > 0) {
            int lastDigit = num % 10;
            reverse += lastDigit;
            num /= 10;
            if (num > 0) reverse *= 10;
        }
        if (Math.abs(number) == reverse) {
            return true;
        }
        return false;
    }
}