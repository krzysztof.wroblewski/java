public class EvenDigitSum {
    public static int getEvenDigitSum(int number) {
        if (number < 0) return -1;
        int sum = 0;
        int num;
        while (number > 0) {
            num = number % 10;
            if (num % 2 == 0) sum += num;
            number /=10;
        }
        return sum;
    }
}
