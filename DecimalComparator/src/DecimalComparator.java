public class DecimalComparator {
    public static boolean areEqualByThreeDecimalPlaces(double x, double y) {
        int new_x = (int) (x * 1000);
        int new_y = (int) (y * 1000);
        if (new_x == new_y) {
            return true;
        }
        return false;

    }
}
