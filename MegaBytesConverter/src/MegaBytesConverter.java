public class MegaBytesConverter {
    public static void printMegaBytesAndKiloBytes(int kilobytes) {
        int megabytes = kilobytes / 1024;
        int remaining_kilobytes = kilobytes % 1024;
        if (kilobytes < 0) {
            System.out.println("Invalid Value");
        } else {
            System.out.println(kilobytes +
                    " KB = " +
                    megabytes +
                    " MB and " +
                    remaining_kilobytes +
                    " KB");
        }
    }
}
